#!/bin/bash
set -u
set -e

echo "[*] Cleaning up temporary data directories"
rm -rf qdata
mkdir -p qdata/logs

echo "[*] Configuring node"
mkdir -p qdata/dd1/{keystore,geth}
cp static-nodes.json qdata/dd1/permissioned-nodes.json
cp static-nodes.json qdata/dd1/
cp keys/key1 qdata/dd1/keystore
cp keys/nodekey1 qdata/dd1/geth/nodekey
geth --datadir qdata/dd1 init istanbul-genesis.json

mkdir -p qdata/dd2/{keystore,geth}
cp static-nodes.json qdata/dd2/permissioned-nodes.json
cp static-nodes.json qdata/dd2/
cp keys/key2 qdata/dd2/keystore
cp keys/nodekey2 qdata/dd2/geth/nodekey
geth --datadir qdata/dd2 init istanbul-genesis.json

mkdir -p qdata/dd3/{keystore,geth}
cp static-nodes.json qdata/dd3/permissioned-nodes.json
cp static-nodes.json qdata/dd3/
cp keys/key3 qdata/dd3/keystore
cp keys/nodekey3 qdata/dd3/geth/nodekey
geth --datadir qdata/dd3 init istanbul-genesis.json
