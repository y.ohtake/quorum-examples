#!/bin/bash
set -u
set -e

GLOBAL_ARGS="--syncmode full --mine --ws --wsaddr 0.0.0.0 --wsapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum,istanbul"

echo "[*] Starting Constellation node"
nohup constellation-node tm1.conf 2>> qdata/logs/constellation1.log &
nohup constellation-node tm2.conf 2>> qdata/logs/constellation2.log &
nohup constellation-node tm3.conf 2>> qdata/logs/constellation3.log &

sleep 4

echo "[*] Starting Quorum node"
PRIVATE_CONFIG=tm1.conf nohup geth --datadir qdata/dd1 $GLOBAL_ARGS --wsport 22000 --wsorigins "*" --port 21000 --unlock 0 --password passwords.txt 2>>qdata/logs/1.log &
PRIVATE_CONFIG=tm2.conf nohup geth --datadir qdata/dd2 $GLOBAL_ARGS --wsport 22001 --wsorigins "*" --port 21001 --unlock 0 --password passwords.txt 2>>qdata/logs/2.log &
PRIVATE_CONFIG=tm3.conf nohup geth --datadir qdata/dd3 $GLOBAL_ARGS --wsport 22002 --wsorigins "*" --port 21002 --unlock 0 --password passwords.txt 2>>qdata/logs/3.log &

echo "[*] Waiting for nodes to start"
echo "All nodes configured. See 'qdata/logs' for logs, and run e.g. 'geth attach qdata/dd1/geth.ipc' to attach to the first Geth node"
