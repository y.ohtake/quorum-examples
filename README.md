# Quorum Examples

## Vagrant Usage

This is a complete Vagrant environment containing Quorum, Constellation, and the
Quorum examples.

### Requirements

  1. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
  2. Install [Vagrant](https://www.vagrantup.com/downloads.html)

### Running

```sh
git clone https://gitlab.com/y.ohtake/quorum-examples
cd quorum-examples
vagrant up
# (should take 5 or so minutes)
vagrant ssh
# Once in the VM environment:
cd quorum-examples
#then simply follow the instructions for the demo you'd like to run.
```

